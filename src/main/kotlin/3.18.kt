import java.util.Scanner

/*
* AUTHOR: Albert Darchiev
* DATE: 2022/11/03
* TITLE: 3.18
*/

fun main(){
    val scanner = Scanner(System.`in`)
    var num = scanner.nextInt()

    var f1 = 0
    var f2 = 0
    var f3 = 1
    for (i in 0 until num){
        println(f1 + f2)
        f1 = f1 +f2
        f2 = f3
        f3 = f1
    }
    println("RESULTAT: ")
}