/*
* AUTHOR: Albert Darchiev
* DATE: 2022/10/14
* TITLE: 4.3 Calcula la lletra del dni

*/

import java.util.*
fun main() {
    var scanner = Scanner(System.`in`)
    println("Introduce el DNI sin letra:")
    var numDNI = scanner.nextInt()
    var dni = numDNI % 23
    val DNInums = arrayOf('T','R','W','A','G','M','Y','F','P','D','X','B','N','J','Z','S','Q','V','H','L','C','K','E')
    println(DNInums[dni])
}

