/*
* AUTHOR: Albert Darchiev
* DATE: 2022/10/08
* TITLE: 3.2 Calcula la suma dels N primers
*/

import java.util.*
fun main() {
    var scanner = Scanner(System.`in`)
    println("Escriu un nombre enter:")
    var num1 = scanner.nextInt()
    var suma = 0

    for (i in 0.. num1){
        suma += i
    }

    println(suma)
    println("FINISH")

}