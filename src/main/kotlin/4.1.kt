/*
* AUTHOR: Albert Darchiev
* DATE: 2022/10/14
* TITLE: 4.1 Suma els valors
*/

import java.util.*
fun main(args: Array<String>) {
    var suma = 0

    for (arg in args) {
        suma += arg.toInt()
    }
    println(suma)
}