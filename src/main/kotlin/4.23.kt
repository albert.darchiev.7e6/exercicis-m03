import java.util.*

/*
* AUTHOR: Albert Darchiev
* DATE: 2022/11/18
* TITLE: 4.23
*/

fun main() {
    val scanner = Scanner(System.`in`)
    println("Introduce una palabra: ")
    val word = scanner.next()
    val length = word.length


    for (i in length-1 downTo 0 ){
        print(word[i])
    }
}