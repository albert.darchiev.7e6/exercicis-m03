import java.util.*

/*
* AUTHOR: Albert Darchiev
* DATE: 2022/11/17
* TITLE: 4.21
*/

fun main() {
    val scanner = Scanner(System.`in`)
    var needed = 0

    println("INPUT: ")
    val string = scanner.next()

    val length = string.length

    if (length % 2 != 0){
        println("ESTA MAL")
        return
    }
        for (i in 0 until length){
            if (needed < 0){
                println("ESTA MAL")
                return
            }
            else if (string[i].toString() == "(" ) needed++
            else if (string[i].toString() == ")" ) needed--
        }
    if (needed == 0) println("ESTA BIEN")
    else println("ESTA MAL")
    }
