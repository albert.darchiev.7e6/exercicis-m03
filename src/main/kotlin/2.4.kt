/*
* AUTHOR: Albert Darchiev
* DATE: 2022/09/29
* TITLE: 2.4 Té edat per treballar
*/

import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("num1:")
    val num1 = scanner.nextInt()

    if (num1 in 16..65){println("Té edat per treballar")}
    else println("No té edat per treballar")
}
