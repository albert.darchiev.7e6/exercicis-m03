/*
* AUTHOR: Albert Darchiev
* DATE: 2022/10/14
* TITLE: 3.10
*/

import java.util.*
fun main() {
    var scanner = Scanner(System.`in`)
    println("Type a number:")
    var num1 = scanner.nextInt()
    var high = num1
    var low = num1



    while (num1 != 0){
        num1 = scanner.nextInt()
        if (num1 < low && num1 != 0) low = num1
        else if (num1 > high && num1 != 0) high = num1

    }
    println("HIGHEST: $high")
    println("LOWER: $low")
}
