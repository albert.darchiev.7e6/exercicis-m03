/*
* AUTHOR: Albert Darchiev
* DATE: 2022/10/19
* TITLE: 3.11
*/
import java.util.Scanner

fun main(){
    var scanner = Scanner(System.`in`)
    println("Introduce un numero:")
    val numINT = scanner.nextInt()
    var result = 0.00

    for (i in 1..numINT){
        result += 1.00 / i
    }
        println(result)
}