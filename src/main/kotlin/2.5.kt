/*
* AUTHOR: Albert Darchiev
* DATE: 2022/09/29
* TITLE: 2.5 En rang
*/

import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("num1:")
    val num1 = scanner.nextInt()
    println("num2:")
    val num2 = scanner.nextInt()
    println("num3:")
    val num3 = scanner.nextInt()
    println("num4:")
    val num4 = scanner.nextInt()
    println("num5:")
    val num5 = scanner.nextInt()

    if (num5 in num1..num2 && num5 in num3..num4){println("true")}
    else println("false")
}
