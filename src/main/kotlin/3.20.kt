import java.util.*

/*
* AUTHOR: Albert Darchiev
* DATE: 2022/10/19
* TITLE: 3.20
*/

fun main() {
    val scanner = Scanner(System.`in`)
    var num1 = scanner.nextInt()
    for (i in 1..num1){
        for (j in 1..i){
            print(j)
        }
        println("")
    }

}