import java.util.Scanner

/*
* AUTHOR: Albert Darchiev
* DATE: 2022/10/19
* TITLE: 3.12 Divisible per 3 i per 5
*/

fun main(){
    val scanner = Scanner(System.`in`)
    println("Introduce un numero:")
    var num1 = scanner.nextInt()
        for (i in 1..num1){
            if(i % 3 == 0 && i % 5 == 0) println("$i ES Divisible entre 3 y 5")
            else if (i % 3 == 0) println("$i ES Divisible entre 3")
            else if (i % 5 == 0) println("$i ES Divisible entre 5")
        }
}