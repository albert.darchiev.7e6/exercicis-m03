/*
* AUTHOR: Albert Darchiev
* DATE: 2022/10/6
* TITLE: 2.10 Calcula la lletra del dni
*/

import java.util.*
import kotlin.math.pow

fun main() {
    var scanner = Scanner(System.`in`)
    println("Introduce tu DNI sin letra/s:")
    var dni1 = scanner.nextInt()
    var dni = dni1 %23
    if (dni == 0) println("$dni -T")
    else if (dni == 1) println("$dni1 -R")
    else if (dni == 2) println("$dni1 -W")
    else if (dni == 3) println("$dni1 -A")
    else if (dni == 4) println("$dni1 -G")
    else if (dni == 5) println("$dni1 -M")
    else if (dni == 6) println("$dni1 -Y")
    else if (dni == 7) println("$dni1 -F")
    else if (dni == 8) println("$dni1 -P")
    else if (dni == 9) println("$dni1 -D")
    else if (dni == 10) println("$dni1 -X")
    else if (dni == 11) println("$dni1 -B")
    else if (dni == 12) println("$dni1 -N")
    else if (dni == 13) println("$dni1 -J")
    else if (dni == 14) println("$dni1 -Z")
    else if (dni == 15) println("$dni1 -S")
    else if (dni == 16) println("$dni1 -Q")
    else if (dni == 17) println("$dni1 -V")
    else if (dni == 18) println("$dni1 -H")
    else if (dni == 19) println("$dni1 -L")
    else if (dni == 20) println("$dni1 -C")
    else if (dni == 21) println("$dni1 -K")
    else println("$dni1 -E")



}