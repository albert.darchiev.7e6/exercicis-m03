/*
* AUTHOR: Albert Darchiev
* DATE: 2022/10/07
* TITLE: 2.14 Any de traspàs
*/

import java.util.*

fun main() {
    var scanner = Scanner(System.`in`)
    println("Introduce el año")
    var year = scanner.nextInt()

    if ((year%4 == 0) && (year%100 !=0 || year%400 == 0) || year%100 !=0) println("SI es de transpaso")
    else println("No es de transpaso")
}