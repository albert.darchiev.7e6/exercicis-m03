/*
* AUTHOR: Albert Darchiev
* DATE: 2022/10/26
* TITLE: 4.13
*/
import java.util.*

fun main(args: Array<String>) {

    var size = args.size-1
    println(size)
    var small = "0"
    var big = "0"

    for (j in 0..size) {
        for (i in 0 until size) {
            if (args[i].toInt() < args[i + 1].toInt()) {
                small = args[i]
                big = args[i + 1]
                args[i] = big
                args[i + 1] = small
            } else if (args[i].toInt() > args[i + 1].toInt()) {
            continue
            }
        }
    }
    for (arg in args) print ("$arg ")
}
