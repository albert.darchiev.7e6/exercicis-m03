/*
* AUTHOR: Albert Darchiev
* DATE: 2022/09/19
* TITLE: 1.4 Calcula l’àrea
*/
import java.util.*
fun main(){
    val scanner = Scanner(System.`in`)
    println("Introdueix la amplada:")
    val numero1 = scanner.nextInt()
    println("Introdueix la llargada:")
    val numero2 = scanner.nextInt()

    println("El teu apartament te una mesura de:")
    print(numero1*numero2)
    print(" m2")

}
