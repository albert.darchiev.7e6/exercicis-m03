/*
* AUTHOR: Albert Darchiev
* DATE: 2022/09/29
* TITLE: 2.9 Canvi mínim
*/

import java.util.*
import kotlin.math.pow

fun main() {
    var scanner = Scanner(System.`in`)
    println("euros:")
    var eur = scanner.nextInt()
    println("centimos:")
    var cent = scanner.nextInt()

    if (cent > 99) print("¡¡Introduce una cifra de centimos INFERIOR a 99!!")
    else {

        println("billete de 500€: " + eur / 500)
        println("Billetes de 200€: " + eur % 500 / 200)
        println("Billetes de 100€:" + eur % 500 % 200 / 100)
        println("Billetes de 50€:" + eur % 500 % 200 % 100 / 50)
        println("Billetes de 20€:" + eur % 500 % 200 % 100 % 50 / 20)
        println("Billetes de 10€:" + eur % 500 % 200 % 100 % 50 % 20 / 10)
        println("Billetes de 5€:" + eur % 500 % 200 % 100 % 50 % 20 % 10 / 5)
        println("Monedas de 2€:" + eur % 500 % 200 % 100 % 50 % 20 % 10 % 5 / 2)
        println("Monedas de 1€" + eur % 500 % 200 % 100 % 50 % 20 % 10 % 5 % 2 / 1)
        println("Monedas de 50cent:" + cent / 50)
        println("Monedas de 20cent:" + cent % 50 / 20)
        println("Monedas de 10cent:" + cent % 50 % 20 / 10)
        println("Monedas de 5cent:" + cent % 50 % 20 % 10 / 5)
        println("Monedas de 2cent:" + cent % 50 % 20 % 10 % 5 / 2)
        println("Monedas de 1cent:" + cent % 50 % 20 % 10 % 5 % 2 / 1)
    }



}
