/*
* AUTHOR: Albert Darchiev
* DATE: 2022/09/29
* TITLE: 2.8 Afegeix un segon (2)
*/

import java.util.*
import kotlin.math.pow

fun main() {
    var scanner = Scanner(System.`in`)
    println("hora:")
    var hora = scanner.nextInt()
    println("min:")
    var min = scanner.nextInt()
    println("sec:")
    var sec = scanner.nextInt()

    if (++sec == 60){
        sec = 0
        min += 1
    }

    if (min == 60){
        min = 0
        hora += 1
    }
    if (hora == 24)
        hora = 0
    val secPrint = if (sec < 10) "0$sec" else "$sec"
    val minPrint = if (min < 10) "0$min" else "$min"
    val horaPrint = if (hora < 10) "0$hora" else "$hora"

    println("$horaPrint:$minPrint:$secPrint")
    }

