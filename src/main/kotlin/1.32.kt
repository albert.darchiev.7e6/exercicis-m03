/*
* AUTHOR: Albert Darchiev
* DATE: 2022/09/22
* TITLE: 1.32 Calcula el capital (no LA capital)
*/
import java.util.*
import kotlin.math.pow
import kotlin.math.sqrt

fun main(){
    val scanner = Scanner(System.`in`)
    println("Capital inicial:")
    val cap = scanner.nextDouble()
    println("Anys :")
    val years = scanner.nextDouble()
    println("Percentatge (entre 0 i 100%):")
    val percent = scanner.nextDouble()


println((cap/100*percent)*years+cap)
}
