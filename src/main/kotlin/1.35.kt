/*
* AUTHOR: Albert Darchiev
* DATE: 2022/09/22
* TITLE: 1.35 Creador de targetes de treball
*/
import java.util.*
import kotlin.math.pow
import kotlin.math.sqrt

fun main(){
    val scanner = Scanner(System.`in`)
    println("Nom:")
    val name = scanner.next()
    println("Cognom:")
    val surn = scanner.next()
    println("Edat:")
    val num = scanner.nextInt()
    print("Empleat/da: $name $surn - Despatx: $num")
}
