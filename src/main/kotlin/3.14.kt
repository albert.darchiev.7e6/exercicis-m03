import java.util.*

/*
* AUTHOR: Albert Darchiev
* DATE: 2022/10/19
* TITLE: 3.14 Endevina el número
*/

fun main(){
    val scanner = Scanner(System.`in`)
    val num1 = null

    val random = (1..100).random()
    println("Introduce un numero:")

        while (num1 != random){
            val num1 = scanner.nextInt()
            if (num1 !in 1..100) println("NUMERO INCORRECTE")
            else if (num1 == random) println("HAS ENCERTAT!!")
            else if (num1 < random) println("Massa baix")
            else if (num1 > random) println("Massa alt")
        }
}