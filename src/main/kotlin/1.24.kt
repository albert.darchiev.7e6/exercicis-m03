/*
* AUTHOR: Albert Darchiev
* DATE: 2022/09/22
* TITLE: 1.24 És un nombre?
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Entrada:")
    val a1 = scanner.next().single()


    println ((a1 >= '0') && (a1 <= '9'))
}
