/*
* AUTHOR: Albert Darchiev
* DATE: 2022/10/26
* TITLE: 4.10 Busca el que falta
*/

import java.util.*
fun main(args: Array<String>) {
    var falta = 0
    for (i in 0 until args.size) {
        if (args[i+1].toInt() == args[i].toInt()+1) continue
        else {
            falta = args[i].toInt()+1
            break
        }
    }
    println("NUMERO QUE FALTA: "+falta)
}
