/*
* AUTHOR: Albert Darchiev
* DATE: 2022/09/22
* TITLE: 1.36 Construeix la història
*/
import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Introdueix 3 frases:")
    val frase1 = scanner.next()
    val frase2 = scanner.next()
    val frase3 = scanner.next()
    print("$frase1 $frase2 $frase3")
}
