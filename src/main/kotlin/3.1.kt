/*
* AUTHOR: Albert Darchiev
* DATE: 2022/10/08
* TITLE: 3.1 Pinta X números
*/

import java.util.*
fun main() {
    var scanner = Scanner(System.`in`)
    println("Escriu un nombre enter:")
    var num1 = scanner.nextInt()

    for (i in 1..num1){
        println(i)
    }
}